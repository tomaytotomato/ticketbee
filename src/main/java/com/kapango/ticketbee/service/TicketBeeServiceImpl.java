package com.kapango.ticketbee.service;

import com.kapango.ticketbee.api.CreateTicketRequest;
import com.kapango.ticketbee.api.TicketResponse;
import com.kapango.ticketbee.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TicketBeeServiceImpl implements TicketBeeService {

    private final TicketRepository repository;

    @Autowired
    public TicketBeeServiceImpl(final TicketRepository repository) {
        this.repository = repository;
    }

    @Override
    public Integer createTicket(final CreateTicketRequest request) {
        return null;
    }

    @Override
    public TicketResponse getTicket(final Integer ticketId) {
        return null;
    }
}
