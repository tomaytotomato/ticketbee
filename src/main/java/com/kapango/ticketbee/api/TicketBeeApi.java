package com.kapango.ticketbee.api;

public interface TicketBeeApi {

    Integer createTicket(CreateTicketRequest request);

    TicketResponse getTicket(Integer ticketId);

}
