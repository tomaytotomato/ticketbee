package com.kapango.ticketbee.controller;

import com.kapango.ticketbee.api.CreateTicketRequest;
import com.kapango.ticketbee.api.TicketBeeApi;
import com.kapango.ticketbee.api.TicketResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class TicketBeeController implements TicketBeeApi {

    @PostMapping("/ticket")
    @Override
    public Integer createTicket(final CreateTicketRequest request) {
        return null;
    }

    @GetMapping("/ticket/{ticketId}")
    @Override
    public TicketResponse getTicket(@PathVariable final Integer ticketId) {
        return null;
    }
}
