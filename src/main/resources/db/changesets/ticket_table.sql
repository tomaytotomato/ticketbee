CREATE TABLE ticket
(
    id int AUTO_INCREMENT PRIMARY KEY NOT NULL,
    description varchar2(255),
    user_id int NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL,
    latitude decimal(10,8) NOT NULL,
    longitude decimal(10,8),
    country varchar2(255) NOT NULL,
    area varchar2(255),
    expires_at timestamp NOT NULL,
    type varchar2(40),
    cost decimal(5,2)
);
CREATE INDEX ticket_created_at_index ON ticket (created_at);
CREATE INDEX ticket_updated_at_index ON ticket (updated_at);
CREATE INDEX ticket_user_id_index ON ticket (user_id);
CREATE INDEX ticket_type_index ON ticket (type);
CREATE INDEX ticket_country_index ON ticket (country);
CREATE INDEX ticket_expires_at_index ON ticket (expires_at)